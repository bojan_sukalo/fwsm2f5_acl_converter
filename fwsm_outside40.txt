access-list outside40 extended permit icmp any any 
access-list outside40 extended permit udp any host 1.7.65.95 eq ntp 
access-list outside40 extended permit udp any host 1.5.95.17 eq ntp 
access-list outside40 extended permit udp any host 1.0.95.12 eq ntp 
access-list outside40 extended permit udp any host 1.0.95.14 eq ntp 
access-list outside40 extended permit udp any host 1.0.9.6 eq ntp 
access-list outside40 extended permit ip any 1.0.84.0 255.255.255.224 
access-list outside40 extended permit ip any 1.0.51.0 255.255.255.192 
access-list outside40 extended permit ip any host 1.0.55.205 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.0.15.122 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.7.1.82 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.0.15.91 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.0.15.81 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.0.47.132 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.6.1.70 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.8.1.9 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.0.25.69 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.0.66.21 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.1.1.29 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.2.65.27 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.2.64.16 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.7.65.124 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.3.64.100 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.5.1.115 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.0.11.98 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.0.26.221 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.0.64.39 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.0.15.83 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.3.65.100 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.3.1.17 
access-list outside40 extended permit ip 1.129.0.0 255.255.0.0 host 1.9.2.5 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.2.0.133 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.6.1.100 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.2.0.113 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.9.0.199 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.3.0.231 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.4.0.18 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.0.51.57 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.0.9.54 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.0.64.23 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.1.1.8 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.9.1.12 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.2.0.253 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.7.66.108 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.8.1.39 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.3.1.21 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.7.1.83 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.0.8.218 
access-list outside40 extended permit ip 1.133.0.0 255.255.0.0 host 1.2.0.211 
access-list outside40 extended permit ip 1.133.0.0 255.255.0.0 host 1.2.64.16 
access-list outside40 extended permit ip 1.133.0.0 255.255.0.0 host 1.3.0.112 
access-list outside40 extended permit ip 1.133.0.0 255.255.0.0 host 1.0.26.221 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 1.0.56.0 255.255.255.224 
access-list outside40 extended permit ip 1.192.16.0 255.255.255.0 host 1.0.25.69 
access-list outside40 extended permit ip 1.192.16.0 255.255.255.0 host 1.0.8.123 
access-list outside40 extended permit ip 1.252.2.0 255.255.255.0 1.0.47.0 255.255.255.0 
access-list outside40 extended permit ip 1.252.2.0 255.255.255.0 host 1.0.98.15 
access-list outside40 extended permit ip 1.252.2.0 255.255.255.0 host 1.0.7.3 
access-list outside40 extended permit ip 1.252.2.0 255.255.255.0 host 1.0.7.2 
access-list outside40 extended permit ip 1.252.2.0 255.255.255.0 host 1.0.26.217 
access-list outside40 extended permit ip 1.252.69.0 255.255.255.0 1.0.47.0 255.255.255.0 
access-list outside40 extended permit ip 3.3.243.0 255.255.255.0 host 1.0.38.63 
access-list outside40 extended permit ip 3.3.243.0 255.255.255.0 host 1.6.16.20 
access-list outside40 extended permit ip 3.3.243.0 255.255.255.0 host 1.0.42.35 
access-list outside40 extended permit ip 3.3.243.0 255.255.255.0 1.0.47.0 255.255.255.0 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 1.0.40.0 255.255.255.0 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 1.0.47.0 255.255.255.0 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.4.10 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.97.8 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.55.35 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.15.111 
access-list outside40 extended permit ip 2.2.128.0 255.255.255.0 1.0.40.0 255.255.255.0 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 1.0.97.0 255.255.255.0 
access-list outside40 extended permit tcp any host 1.0.40.66 eq smtp 
access-list outside40 extended permit ip 1.0.100.0 255.255.255.0 any 
access-list outside40 extended permit ip 1.130.0.0 255.255.0.0 host 1.0.35.229 
access-list outside40 extended permit tcp 2.2.0.0 255.255.255.0 host 1.0.61.66 eq smtp 
access-list outside40 extended permit tcp any host 1.0.61.66 eq smtp 
access-list outside40 extended permit tcp any host 1.0.61.67 eq smtp 
access-list outside40 extended permit tcp host 3.3.243.60 host 1.0.55.205 eq 4343 
access-list outside40 extended permit tcp host 2.2.0.52 host 1.0.40.230 eq 1918 
access-list outside40 extended permit ip 1.193.0.0 255.255.254.0 host 1.0.51.21 
access-list outside40 extended permit ip 1.193.0.0 255.255.254.0 host 1.0.51.6 
access-list outside40 extended permit ip 1.193.0.0 255.255.254.0 host 1.0.51.52 
access-list outside40 extended permit ip 1.193.0.0 255.255.254.0 host 1.0.51.44 
access-list outside40 extended permit ip 1.193.0.0 255.255.254.0 host 1.0.51.33 
access-list outside40 extended permit ip 1.193.0.0 255.255.254.0 host 1.0.51.29 
access-list outside40 extended permit ip 1.193.0.0 255.255.254.0 host 1.0.25.27 
access-list outside40 extended permit ip 1.193.0.0 255.255.254.0 host 1.0.25.22 
access-list outside40 extended permit ip 1.193.0.0 255.255.254.0 host 1.0.25.18 
access-list outside40 extended permit ip 1.193.0.0 255.255.254.0 host 1.0.25.19 
access-list outside40 extended permit ip 1.193.0.0 255.255.254.0 host 1.0.25.17 
access-list outside40 extended permit ip 3.3.243.0 255.255.255.0 host 1.0.14.252 
access-list outside40 extended permit ip host 1.130.0.3 host 1.0.55.156 
access-list outside40 extended permit ip 1.132.0.0 255.255.0.0 host 2.2.98.102 
access-list outside40 extended permit ip 1.132.0.0 255.255.0.0 host 1.0.151.3 
access-list outside40 extended permit tcp host 2.2.0.42 host 1.0.111.35 eq www 
access-list outside40 extended permit tcp host 2.2.0.142 host 1.0.111.35 eq www 
access-list outside40 extended permit tcp host 2.2.0.42 host 1.0.60.213 eq www 
access-list outside40 extended permit tcp host 2.2.0.142 host 1.0.60.213 eq www 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.60.218 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.56.2 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.56.6 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.42.41 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.54.110 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.54.111 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.54.11 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.54.112 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.56.110 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.56.111 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.56.112 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.56.113 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.56.100 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.56.101 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.56.102 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 host 1.0.56.103 
access-list outside40 extended permit tcp 2.2.0.0 255.255.255.0 1.0.40.0 255.255.255.248 eq ldap 
access-list outside40 extended permit ip 2.2.0.0 255.255.255.0 1.15.193.0 255.255.255.0 
access-list outside40 extended permit ip host 2.2.0.142 host 1.0.40.230 
access-list outside40 extended permit ip host 2.2.0.142 host 1.0.97.8 
access-list outside40 extended permit tcp host 2.2.0.42 host 1.0.40.2 eq 1918 
access-list outside40 extended permit tcp host 2.2.0.42 host 1.0.40.2 eq 6014 
access-list outside40 extended permit tcp host 2.2.0.42 host 1.0.40.2 eq 63358 
access-list outside40 extended permit tcp host 2.2.0.60 host 1.0.40.2 eq 1918 
access-list outside40 extended permit tcp host 2.2.0.60 host 1.0.40.2 eq 6014 
access-list outside40 extended permit tcp host 2.2.0.60 host 1.0.40.2 eq 63358 
access-list outside40 extended permit tcp host 2.2.0.66 1.0.60.208 255.255.255.248 eq 1918 
access-list outside40 extended permit tcp host 2.2.0.66 1.0.60.208 255.255.255.248 eq 6014 
access-list outside40 extended permit tcp host 2.2.0.66 1.0.60.208 255.255.255.248 eq 63358 
access-list outside40 extended permit ip host 2.2.0.62 host 1.0.19.20 
access-list outside40 extended permit ip host 2.2.0.64 host 1.0.19.20 
access-list outside40 extended permit ip host 2.2.0.66 host 1.0.19.20 
access-list outside40 extended permit ip host 2.2.0.60 host 1.0.19.20 
access-list outside40 extended permit ip host 2.2.0.42 host 1.0.19.20 
access-list outside40 extended permit ip host 2.2.0.46 host 1.0.19.20 
access-list outside40 extended permit tcp 2.2.0.0 255.255.255.0 host 1.0.19.57 eq ssh
