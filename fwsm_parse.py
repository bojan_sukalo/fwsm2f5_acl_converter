def fwsm_acl_line2dict(acl):
	""" 
	Creates dictionary from fwsm_acl line
	Here's the example of fwsm acl line
	access-list outside130 permit udp 192.168.60.0 255.255.255.240 host 1.1.1.1 eq www log 
	As port and log are optional parameters, if they are not present their value is 'None' """
	acl_template={'protocol': '', 'log': '', 'acl_name': '', 'destination': '', 'source': '', 'action': '', 'port': ''}
	import re
	import portnames
	import ipaddr
	#regexes for parts of acl, if more appropriate regexes are needed or you want to 
	#check whether IPs are valid here's the place for change
	#also if your access-list has another name part from outsidexx you should change regex re_acl_name to something more appropriate
	re_acl_name = r'access-list (?P<acl_name>\w+)'
	re_action = r'extended (?P<action>\w+)'
	re_protocol = r'(?P<protocol>(ah|eigrp|esp|gre|icmp|igmp|ip|ospf|pim|snp|tcp|udp))'
	re_src = r'(?P<source>((host \d+.\d+.\d+.\d+)|(\d+.\d+.\d+.\d+ \d+.\d+.\d+.\d+)|any))'
	re_dst = r'(?P<destination>((host \d+.\d+.\d+.\d+)|(\d+.\d+.\d+.\d+ \d+.\d+.\d+.\d+)|any))'
	re_port = r'( eq (?P<port>(\d+)|(\w+)))?'
	re_log = r'( (?P<log>log))?'
	#joining mandatory fields
	re_acl = re_acl_name+" "+re_action+" "+re_protocol+" "+re_src+" "+re_dst+" "
	#joining optional fields, port and log
	re_acl_port_log = re_acl+re_port+re_log
	m = re.match(re_acl_port_log,acl)
	#dictionary format must be normalised
	try:
		acl_dict = m.groupdict()
	except AttributeError:
		print "There is a problem with line: "
		print(acl)
		print("I'll quit")
		quit()
	#
	#changing format of dict values, removing host string and makig cidr notation
	#
	if "host" in acl_dict['destination']:
		acl_dict['destination'] = acl_dict['destination'][5:]
	if "host" in acl_dict['source']:
		acl_dict['source'] = acl_dict['source'][5:]
	if acl_dict['port']:
		if re.search(r'[^\d]+', acl_dict['port']):
			acl_dict['port']=portnames.port_name_translator[acl_dict['port']]
	if re.search(r'\d+.\d+.\d+.\d+ \d+.\d+.\d+.\d+', acl_dict['source']):
		acl_dict['source']="/".join(acl_dict['source'].split())
	if re.search(r'\d+.\d+.\d+.\d+ \d+.\d+.\d+.\d+', acl_dict['destination']):
		acl_dict['destination']="/".join(acl_dict['destination'].split())
	if re.search(r'any', acl_dict['source']):
		acl_dict['source']="0.0.0.0/0"
	if re.search(r'any', acl_dict['destination']):
		acl_dict['destination']="0.0.0.0/0"
	try:
		acl_dict['destination']=str(ipaddr.IPNetwork(acl_dict['destination'],strict=False))
	except ValueError:
		print "postoji problem sa mrezom "+acl_dict['destination']
		print "prekidam obradu"
		quit()
	try:
		acl_dict['source']=str(ipaddr.IPNetwork(acl_dict['source'],strict=False))
	except ValueError:
		print "Some sort of problem occured with network notation on line with "+acl_dict['destination']
		print "I'll quit!"
		quit()
	#assign values to template dict
	for i in acl_dict.keys():
		acl_template[i]=acl_dict[i]
	return acl_template;


def acl_to_f5_format (acl_dict, policy_name='', rule_name=''):
	"""Converts dictionary format acl into f5 format
	If no rule_name is given it will create random one from 12 lowercase ascii chars"""
	f5_acl=''
	from random import choice
	from re import sub
	import string
	if not policy_name:
		policy_name=acl_dict['acl_name']
	if not rule_name:
		rule_name=''.join(choice(string.ascii_lowercase) for i in range(12))
	f5_acl_template = r'modify security firewall policy {0} rules add {{ '\
	'{1} {{place-after last '\
	'source {{ addresses add {{{2}}}}} '\
	'destination {{addresses add {{{3}}}}} '\
	'ip-protocol {4} action {5} }}}}'
	if "permit" in acl_dict['action']:
		acl_dict['action'] = 'accept'
	if "deny" in acl_dict['action']:
		acl_dict['action'] = 'drop'
	if acl_dict['log']:
		f5_acl_template = f5_acl_template[:-4]+"log yes "+ f5_acl_template[-4:]
	if acl_dict['port']:
		f5_acl_template = sub(r'}} ip-protocol',r' ports add {{{6}}}}} ip-protocol', f5_acl_template)
		f5_acl = f5_acl_template.format(policy_name,rule_name, acl_dict['source'], acl_dict['destination'], acl_dict['protocol'],acl_dict['action'],acl_dict['port'])
	if not f5_acl:
		f5_acl=f5_acl_template.format(policy_name,rule_name, acl_dict['source'], acl_dict['destination'], acl_dict['protocol'],acl_dict['action'])
	return f5_acl





