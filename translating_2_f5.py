fwsm_file = 'fwsm_outside40.txt'
f5_file = 'f5_outside40.txt'
import fwsm_parse
import portnames
from_file=open(fwsm_file, 'r')
to_file=open(f5_file,'w')

for line in from_file:
	acl_dict = fwsm_parse.fwsm_acl_line2dict(line)
	f5_acl_line = fwsm_parse.acl_to_f5_format (acl_dict, policy_name='Test_policy')
	to_file.write(f5_acl_line)
	to_file.write("\n")

from_file.close()
to_file.close()
